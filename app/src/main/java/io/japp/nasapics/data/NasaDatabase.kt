package io.japp.nasapics.data

import androidx.room.Database
import androidx.room.RoomDatabase
import io.japp.nasapics.NasaData
import io.japp.nasapics.di.ApplicationScope
import kotlinx.coroutines.CoroutineScope
import javax.inject.Inject
import javax.inject.Provider

@Database(entities = [NasaData::class], version = 1)
abstract class NasaDatabase : RoomDatabase() {

    abstract fun nasaDao(): NasaDao

    class CallBack @Inject constructor(
        private val database: Provider<NasaDatabase>,
        @ApplicationScope private val applicationScope: CoroutineScope
    ) : RoomDatabase.Callback() {
    }
}