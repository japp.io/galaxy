package io.japp.nasapics.data

import androidx.room.*
import io.japp.nasapics.NasaData
import kotlinx.coroutines.flow.Flow

@Dao
interface NasaDao {

    @Query("SELECT * FROM nasa_table")
    fun getNasaData(): Flow<List<NasaData>>

    @Query("SELECT * FROM nasa_table WHERE isBookMarked ==  1")
    fun getBookmarkedData(): Flow<List<NasaData>>

    @Update
    suspend fun update(nasaData: NasaData)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(nasaData: List<NasaData>)

}