package io.japp.nasapics

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize


@Entity(tableName = "nasa_table")
@Parcelize
data class NasaData(
    @SerializedName("copyright") val copyright : String,
    @PrimaryKey
    @SerializedName("date") val date : String,
    @SerializedName("explanation") val explanation : String,
    @SerializedName("hdurl") val hdUrl : String,
    @SerializedName("media_type") val mediaType : String,
    @SerializedName("service_version") val serviceVersion : String,
    @SerializedName("title") val title : String,
    @SerializedName("url") val url : String,
    val isBookMarked: Boolean = false
) : Parcelable

@Parcelize
class NasaDataList: ArrayList<NasaData>(), Parcelable