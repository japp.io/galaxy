package io.japp.nasapics.utils

import java.text.SimpleDateFormat
import java.util.*

object Utils {
    fun getFormattedDate(dateString: String): String {
        val date = SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(dateString)
        val oi = getOrdinalIndicator(date)
        return SimpleDateFormat("MMM d'$oi', yyyy", Locale.ENGLISH).format(date)
    }

    private fun getOrdinalIndicator(date: Date): String {
        val day = newCalendar(date).get(Calendar.DAY_OF_MONTH)
        if (day == 11 || day == 12 || day == 13) {
            return "th"
        }
        return when (day % 10) {
            1 -> "st"
            2 -> "nd"
            3 -> "rd"
            else -> "th"
        }
    }

    private fun newCalendar(date: Date): Calendar {
        return Calendar.getInstance().apply {
            time = date
        }
    }
}