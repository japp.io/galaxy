package io.japp.nasapics.ui.main

import android.app.Application
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import io.japp.nasapics.NasaData
import io.japp.nasapics.data.NasaDao
import kotlinx.coroutines.launch
import java.lang.reflect.Type

//@HiltViewModel
class MainViewModel @ViewModelInject constructor(
    application: Application,
    private val nasaDao: NasaDao
) : AndroidViewModel(application) {
    private val json = application.resources.assets.open("data.json").bufferedReader().use {
        it.readText()
    }
    private val itemType: Type = object : TypeToken<ArrayList<NasaData>>() {}.type
    private val itemList: ArrayList<NasaData> = Gson().fromJson(json, itemType)
    val sortedList: ArrayList<NasaData> = ArrayList(itemList.sortedByDescending { it.date })
    private fun insertData(nasaDataList: List<NasaData>) {
        viewModelScope.launch {
            nasaDao.insert(nasaDataList)
        }
    }

    init {
        insertData(sortedList)
    }
}
