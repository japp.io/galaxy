package io.japp.nasapics.ui.detail

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.core.view.doOnLayout
import androidx.fragment.app.Fragment
import androidx.viewpager2.widget.ViewPager2
import dagger.hilt.android.AndroidEntryPoint
import io.japp.nasapics.R
import io.japp.nasapics.databinding.FragmentDetailBinding
import io.japp.nasapics.ui.MainActivity

private const val TAG = "DetailFragment"

@AndroidEntryPoint
class DetailFragment : Fragment(R.layout.fragment_detail) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val binding = FragmentDetailBinding.bind(view)
        val detailAdapter = DetailAdapter()
        binding.apply {
            detailViewPager.orientation = ViewPager2.ORIENTATION_HORIZONTAL
            detailViewPager.adapter = detailAdapter
            val itemList = DetailFragmentArgs.fromBundle(requireArguments()).itemList
            val pos = DetailFragmentArgs.fromBundle(requireArguments()).position
            detailAdapter.submitList(itemList)
            Log.d(TAG, "onViewCreated: position $pos")
            detailViewPager.doOnLayout {
                detailViewPager.setCurrentItem(pos, false)
            }
        }
    }

    override fun onResume() {
        super.onResume()
        (activity as MainActivity).hideStatusBar()
    }
}