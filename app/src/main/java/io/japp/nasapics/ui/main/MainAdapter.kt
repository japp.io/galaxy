package io.japp.nasapics.ui.main

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Color
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.palette.graphics.Palette
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.load.resource.bitmap.BitmapTransitionOptions
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import io.japp.nasapics.NasaData
import io.japp.nasapics.R
import io.japp.nasapics.databinding.ItemGridBinding


private const val TAG = "MainAdapter"

class MainAdapter(private val listener: OnItemClickListener) :
    ListAdapter<NasaData, MainAdapter.MainViewHolder>(DiffCallback()) {

    interface OnItemClickListener {
        fun onItemClick(data: NasaData, position: Int)
    }

    class DiffCallback : DiffUtil.ItemCallback<NasaData>() {
        override fun areItemsTheSame(oldItem: NasaData, newItem: NasaData) =
            oldItem.title == newItem.title

        override fun areContentsTheSame(oldItem: NasaData, newItem: NasaData) =
            oldItem == newItem
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainViewHolder {
        val binding = ItemGridBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return MainViewHolder(binding, parent.context)
    }

    override fun onBindViewHolder(holder: MainViewHolder, position: Int) {
        val currentItem = getItem(position)
        holder.bind(currentItem)
    }

    inner class MainViewHolder(
        private val binding : ItemGridBinding,
        private val context: Context
        ) :
        RecyclerView.ViewHolder(binding.root){

        init {
            binding.apply {
                root.setOnClickListener {
                    listener.onItemClick(getItem(adapterPosition), adapterPosition)
                }
            }
        }

        fun bind(nasaData: NasaData){
            binding.apply {
                Log.d(TAG, "bind: ${nasaData.url}")
//                Glide.with(context).load(nasaData.url).into(gridImageView)
                Glide.with(context).asBitmap().load(nasaData.url)
                    .placeholder(R.drawable.placeholder)
                    .transition(BitmapTransitionOptions.withCrossFade())
                    .listener(object : RequestListener<Bitmap> {
                        override fun onLoadFailed(
                            e: GlideException?,
                            model: Any?,
                            target: Target<Bitmap>?,
                            isFirstResource: Boolean
                        ): Boolean {
                            return false
                        }

                        override fun onResourceReady(
                            resource: Bitmap,
                            model: Any?,
                            target: Target<Bitmap>?,
                            dataSource: DataSource?,
                            isFirstResource: Boolean
                        ): Boolean {
                            Palette.from(resource).generate {
                                val color = it?.darkVibrantSwatch?.rgb ?: it?.darkMutedSwatch?.rgb
                                ?: Color.TRANSPARENT
                                mainTitleBg.setBackgroundColor(color)
                                mainTitleBg.alpha = 0f
                                mainTitleBg.animate().alpha(1f).setDuration(200).start()
                            }
                            return false
                        }

                    })
                    .into(gridImageView)
                mainTitleTv.text = nasaData.title
            }
        }
    }
}