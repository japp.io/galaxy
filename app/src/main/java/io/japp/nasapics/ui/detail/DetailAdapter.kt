package io.japp.nasapics.ui.detail

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewAnimationUtils
import android.view.ViewGroup
import androidx.core.view.doOnLayout
import androidx.palette.graphics.Palette
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.load.resource.bitmap.BitmapTransitionOptions
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.facebook.shimmer.Shimmer
import com.facebook.shimmer.ShimmerDrawable
import io.japp.nasapics.NasaData
import io.japp.nasapics.databinding.ItemDetailBinding
import io.japp.nasapics.utils.Utils
import kotlin.math.hypot

private const val TAG = "DetailAdapter"

class DetailAdapter :
    ListAdapter<NasaData, DetailAdapter.DetailViewHolder>(DiffCallback()) {

    class DiffCallback : DiffUtil.ItemCallback<NasaData>() {
        override fun areItemsTheSame(oldItem: NasaData, newItem: NasaData) =
            oldItem.title == newItem.title

        override fun areContentsTheSame(oldItem: NasaData, newItem: NasaData) =
            oldItem == newItem
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DetailViewHolder {
        val binding = ItemDetailBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return DetailViewHolder(binding, parent.context)
    }

    override fun onBindViewHolder(holder: DetailViewHolder, position: Int) {
        val currentItem = getItem(position)
        holder.bind(currentItem)
    }

    inner class DetailViewHolder(
        private val binding : ItemDetailBinding,
        private val context: Context
    ) :
        RecyclerView.ViewHolder(binding.root){
            private val shimmer: Shimmer = Shimmer.AlphaHighlightBuilder()
                .setDuration(1800)
                .setBaseAlpha(0.7f)
                .setHighlightAlpha(0.6f)
                .setDirection(Shimmer.Direction.LEFT_TO_RIGHT)
                .setAutoStart(true)
                .build()
            private val shimmerDrawable = ShimmerDrawable().apply {
                setShimmer(shimmer)
            }
        fun bind(nasaData: NasaData){
            binding.apply {
                Glide.with(context).asBitmap().load(nasaData.url)
                    .placeholder(shimmerDrawable)
                    .transition(BitmapTransitionOptions.withCrossFade())
                    .listener(object : RequestListener<Bitmap> {
                        override fun onLoadFailed(
                            e: GlideException?,
                            model: Any?,
                            target: Target<Bitmap>?,
                            isFirstResource: Boolean
                        ): Boolean {
                            return false
                        }

                        override fun onResourceReady(
                            resource: Bitmap,
                            model: Any?,
                            target: Target<Bitmap>?,
                            dataSource: DataSource?,
                            isFirstResource: Boolean
                        ): Boolean {
                            Palette.from(resource).generate {
                                val color = it?.darkVibrantSwatch?.rgb ?: it?.darkMutedSwatch?.rgb
                                ?: Color.TRANSPARENT
                                titleBg.setBackgroundColor(Color.BLACK)
                                titleBg.alpha = 0.2f
                                descriptionBg.setBackgroundColor(color)
                                descriptionBg.doOnLayout {
                                    val cx = descriptionBg.width / 2
                                    val cy = descriptionBg.height / 3
                                    val finalRadius =
                                        hypot(cx.toDouble(), 2 * cy.toDouble()).toFloat()
                                    val anim = ViewAnimationUtils.createCircularReveal(
                                        descriptionBg,
                                        cx,
                                        cy,
                                        0f,
                                        finalRadius
                                    )
                                    anim.setDuration(1000).start()
                                }
                            }
                            return false
                        }

                    })
                    .into(imageView)
                nasaTitleTv.text = nasaData.title
                nasaDescriptionTv.text = nasaData.explanation
                copyrightTv.text = nasaData.copyright
//                dateTv.text = nasaData.date
                dateTv.text = Utils.getFormattedDate(nasaData.date)
                if (nasaData.copyright == null || nasaData.copyright.isBlank()) {
                    copyrightTv.text = "Unknown"
                }
            }
        }
    }
}