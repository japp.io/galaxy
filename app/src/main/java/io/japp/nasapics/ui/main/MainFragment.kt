package io.japp.nasapics.ui.main

import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import io.japp.nasapics.NasaData
import io.japp.nasapics.NasaDataList
import io.japp.nasapics.R
import io.japp.nasapics.databinding.FragmentMainBinding
import io.japp.nasapics.ui.MainActivity


private const val TAG = "FragmentMain"

@AndroidEntryPoint
class FragmentMain : Fragment(R.layout.fragment_main), MainAdapter.OnItemClickListener {

    private val viewModel: MainViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val toolbar: Toolbar = view.findViewById(R.id.materialToolbar)
        (activity as MainActivity).setSupportActionBar(toolbar)
        val layoutParams = (toolbar.layoutParams as? ViewGroup.MarginLayoutParams)
        layoutParams?.setMargins(0, getStatusBarHeight(), 0, 0)
        toolbar.layoutParams = layoutParams

        val binding = FragmentMainBinding.bind(view)
        val mainAdapter = MainAdapter(this)
        binding.apply {
            gridRv.apply {
                adapter = mainAdapter
                layoutManager = GridLayoutManager(requireContext(), 2)
                setHasFixedSize(true)
            }
        }
        mainAdapter.submitList(viewModel.sortedList)
    }

    override fun onItemClick(data: NasaData, position: Int) {
        val nasaDataList = NasaDataList()
        nasaDataList.addAll(viewModel.sortedList)
        findNavController().navigate(
            FragmentMainDirections.actionFragmentMainToDetailFragment(
                position,
                nasaDataList
            )
        )
    }

    private fun getStatusBarHeight(): Int {
        var result = 0
        val resourceId = resources.getIdentifier("status_bar_height", "dimen", "android")
        if (resourceId > 0) {
            result = resources.getDimensionPixelSize(resourceId)
        }
        return result
    }

    override fun onResume() {
        super.onResume()
        (activity as MainActivity).showStatusBar()
    }
}