# Galaxy

**Beautiful app to view Galaxy images by NASA.**

![screenshot 1](https://gitlab.com/japp.io/galaxy/-/raw/master/raw/ss1.jpg)
![screenshot 1](https://gitlab.com/japp.io/galaxy/-/raw/master/raw/ss2.jpg)

## App architecture
Model-View-ViewModel

## Packages
- **data**: It contains all the data accessing and manipulating components.
- **ui**: It contains all the view classes along with their corresponding ViewModel.
- **utils**: It contains the utility classes.

## Jetpack components
- [Navigation](https://developer.android.com/jetpack/androidx/releases/navigation)
- [View binding](https://developer.android.com/topic/libraries/view-binding)
- [HILT](https://developer.android.com/jetpack/androidx/releases/hilt)
- [Lifecycle](https://developer.android.com/jetpack/androidx/releases/lifecycle)

## Libraries used
- [Gson](https://github.com/google/gson)
- [Glide](https://github.com/bumptech/glide)
- [Shimmer](https://github.com/facebook/shimmer-android)

## Licenses
```
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
```
